/**
 * CharConnections.cpp
 * Sean Castillo
 */

#include "CharacterGraph.hpp"
using namespace Modes;

/**
 *  argv[0]: program name
 *  argv[1]: data input file
 *  argv[2]: pair input file
 *  argv[3]: bfs/ufind mode select
 *  argv[4]: output file
 */
 int main(int argc, char* argv[])
 {
     char* dataInput;
     char* pairInput;
     s_string mode = "";
     char* outfileName;

     if (argc != 5)
     {
         if (argc == 4) // This allows the last entry to be taken as the filename even if the mode selection is left blank.
         {
             std::cout << "Defaulting to ufind mode." << std::endl;
             mode        = "ufind";
             outfileName = argv[3];
         }
         else
         {
             std::cerr << "Error: Incorrect number of arguments. Expected 3 (defaults to ufind) or 4." << std::endl;
             std::cout << "1. data input file" << '\n'
                       << "2. pair input file" << '\n'
                       << "3. bfs/ufind"       << '\n'
                       << "4. output file"     << '\n';
             return -1;
         }
     }
     else
     {
         mode        = argv[3];
         outfileName = argv[4];
     }

     dataInput = argv[1];
     pairInput = argv[2];
     CharacterGraph cg;
     cg.setTaskMode         (Mode_Task::Task_CharacterConnections);
     cg.setSearchMode       (mode);
     cg.load                (dataInput);
     cg.connectCharsToGames (pairInput, outfileName);

     return 0;
 }
