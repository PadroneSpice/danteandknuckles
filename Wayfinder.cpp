/**
 * Wayfinder.cpp
 */

#include <iostream>
#include <string>
#include "CharacterGraph.hpp"
using namespace Modes;

/**
 *  argv[0]: program name
 *  argv[1]: data input file
 *  argv[2]: pair input file
 *  argv[3]: weighted/unweighted mode select
 *  argv[4]: output file
 */
int omain(int argc, char* argv[])
{
    char* dataInput;
    char* pairInput;
    s_string mode = "";
    char* outfileName;

    if (argc != 5)
    {
        if (argc == 4)  // This allows the last entry to be taken as the filename even if the mode selection is left blank.
        {
            std::cout << "Defaulting to unweighted mode." << std::endl;
            mode = "unweighted";
            outfileName = argv[3];
        }

        else
        {
            std::cerr << "Error: Incorrect number of arguments; expected 3 (defaults to unweighted) or 4." << '\n'
                << "1- data input file" << '\n'
                << "2- pair input file" << '\n'
                << "3- (w)eighted/(u)nweighted mode select" << '\n'
                << "4- result output file" << '\n';
            return -1;
        }
    }
    else
    {
        mode        = argv[3];
        outfileName = argv[4];
    }

    dataInput = argv[1];
    pairInput = argv[2];
    CharacterGraph cg;
    cg.setTaskMode  ( Mode_Task::Task_Wayfinder );
    cg.setEdgeMode  ( mode                      );
    cg.load         ( dataInput                 );
    cg.generateWays ( pairInput, outfileName    );

     return 0;
}
