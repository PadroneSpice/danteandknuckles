/*
 * CharacterGraph.hpp
 * by Sean Castillo
 */

#ifndef CHARACTERGRAPH_HPP
#define CHARACTERGRAPH_HPP

#include <iostream>
#include <stack>
#include <queue>
#include <sstream>
#include <iterator>
#include <fstream>
#include <algorithm>
#include "Node.hpp"

using c_queue        = std::queue<Character*>;
using c_vector       = std::vector<Character*>;
using g_vector       = std::vector<Game*>;
using s_vector       = std::vector<s_string>;
using sv_vector      = std::vector<s_vector>;
using sp_vector      = std::vector<std::pair<s_string, s_string>>;
using i_ifstream     = std::ifstream;
using o_ofstream     = std::ofstream;
using s_stringstream = std::stringstream;
using c_stack        = std::stack<Character*>;
using sp_stack       = std::stack<std::pair<s_string,s_string>>;
using g_stack        = std::stack<Game*>;
using c_map          = std::unordered_map<s_string, Character*>;
using g_map          = std::unordered_map<si_pair, Game*, SimpleHash>;
using s_map          = std::unordered_map<s_string, s_string>;


const unsigned int DATA_CATEGORY_COUNT                 = 3;
const unsigned int DATA_CATEGORY_COUNT_CONDITION_GIVEN = 4;
const unsigned int AKA_COLUMN_COUNT                    = 2;
const unsigned int CHARACTER_PAIR_COUNT                = 2;
const          int MAX_DISTANCE         = std::numeric_limits<int>::max();


namespace Modes
{
    /* Wayfinder or Character Connections */
    enum class Mode_Task
    {
        Task_Wayfinder, Task_CharacterConnections
    };

    /* Unweighted Edges or Weighted Edges */
    enum class Mode_Edge
    {
        Edge_Unweighted, Edge_Weighted
    };

    /* Union Find or Breadth-First Search */
    enum class Mode_Search
    {
        Search_UF, Search_BFS
    };

}


class CharacterGraph
{
    public:
        CharacterGraph();

        /* ModeSetup.cpp */
        void setTaskMode(Modes::Mode_Task mode);
        void setSearchMode(s_string mode);
        void setEdgeMode(s_string mode);


        /* CharacterGraph.cpp */
        void load                 (const char* inputFile);
        void connectCharsToGames  (const char* inputFile, const char* outputFile);
        void generateWays         (const char* inputPairFile, const char* outputFile);
        ~CharacterGraph();

    private:
        /* Note that visitedCharStack is not the same as resetCharStackVisited().
         * visitedCharStack is a stack for char() that keeps track of
         * Character nodes visited during BFS.
         * resetCharStackVisited() is a function that flags all the Character
         * nodes in a given stack as visited = false.
         */


        /* ErrorMessages.cpp */
        void       printErrorMessage (u_int codeNumber);
        void       printErrorMessage (u_int codeNumber, s_vector v);

        /* Search Algorithms defined in SearchAlgorithms.cpp */
        bool       BFS              (const s_string charA, const s_string charB, o_ofstream& outputFile,
                                     bool handlePrint);
        bool       Dijkstra         (const s_string charA, const s_string charB, o_ofstream& outputFile);


        /* Algorithm Utilities */
        s_vector  tokenizeString    (const s_string inputString, const s_string del = "%");
        sv_vector buildRecord       (const char* inputFile);
        void processData            (s_string characterName, s_string gameName, int gameYear, s_string characterCondition);
        Character* unionFind        (Character* c);
        bool charsAreUnioned        (Character*  a, Character*  b);
        void makeUnion              (Character*& a, Character*& b);
        bool hasChar                (s_string charaterName) const;
        bool hasChar                (const s_string characterName, Character*& CharPtr);
        void reverseStack           (sp_stack s);
        u_int pushGameStackContents ();
        void resetCharStackVisited  (c_stack& c);
        void resetParentsInStack    (c_stack& c);
        void resetCharAppearances   ();
        void resetForAlgorithms     ();
        void printUnion             (const s_string charA, const s_string charB, const u_int year, o_ofstream& outputFile);
        void printUnion             (const s_string charA, const s_string charB,                   o_ofstream& outputFile);
        void printTheWay            (Character* curr, const s_string charA, o_ofstream& outputFile);
        void printVector            (g_vector g);

        /* Member data used for search algorithms */
        c_stack        visitedCharStack;
        g_stack        gamePrevCharStack;
        c_map          charMap;
        g_map          gameMap;
        s_map          akaMap;
        g_vector       gameList;
        c_vector       charList;
        sp_vector      akaList;
        g_stack        gameStack;
        c_stack        parentStack;
        sp_stack       akaStack;

        /* Program Modes */
        Modes::Mode_Task      taskMode;
        Modes::Mode_Edge      edgeMode;
        Modes::Mode_Search    searchMode;

};


#endif // CHARACTERGRAPH_HPP
