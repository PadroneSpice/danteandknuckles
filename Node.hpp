/**
 * Node.hpp
 * by Sean Castillo
 */

#ifndef NODE_HPP
#define NODE_HPP

#include <limits>
#include <vector>
#include <string>
#include <iostream>
#include <unordered_map>


class Game;
class Character;      // forward declarations

using u_int    = unsigned int;
using s_string = std::string;
using si_pair  = std::pair<s_string, int>;
struct SimpleHash
{
    size_t operator()(const si_pair& p) const
    {
        return std::hash<s_string>()(p.first) ^
            std::hash<int>()(p.second);
    }
};
using c_map    = std::unordered_map<s_string, Character*>;
using g_map    = std::unordered_map<si_pair, Game*, SimpleHash>;
using condition_map = std::unordered_map<s_string, s_string>; // Game Name and Condition

const int CURRENT_YEAR = 2022; //not yet sure how to use ctime to make this always the right year

class Node
{
    public:
        Node() {}
        s_string     getName()      const { return name;      }
        s_string     getCondition() const { return condition; }
        //void         setName(s_string s) { name = s;    }
    protected:
        s_string     name;
        s_string     condition;
 };

class Character : public Node
{
    public:
        Character(s_string n)
        {
            name      = n;
            distance  = std::numeric_limits<int>::max(); //represents infinity
            prevGame  = nullptr;
            visited   = false;
            P         = this;
            size      = 1;
        }

        Game*         prevGame;
        bool          visited;
        u_int         distance;
        u_int         size;
        g_map         appearances;
        condition_map appearanceConditions;

        Character* P; // P is for Parent. This is for the Disjoint Set system.
                      // It allows characters to point to other characters,
                      // just for that application.

        void addAppearance (Game*& g);
        void addAppearanceCondition(s_string gameName, s_string condition)
        {
            std::cout << "addAppearanceCondition(): " << gameName << " " << condition << std::endl;
            appearanceConditions.insert(std::make_pair(gameName, condition));
        }
        void setPrevGame   (Game*  g)  { prevGame = g; }
        void setDistance   (u_int  d)  { distance = d; }


        bool operator() (Character* a, Character* b)
        {
            return a->distance > b->distance;
        }

    private:


};


class Game : public Node
{
    public:
        Game(s_string n, u_int y)
        {
            name          = n;
            year          = y;
            prevChar      = nullptr;
            weight        = 1 + (CURRENT_YEAR - y);
        }
        Character*      prevChar;
        c_map roster;
        void  addChar(Character*& c);
        u_int getYear     ()       const  { return year;   }
        u_int getWeight   ()       const  { return weight; }
        void  setPrevChar (Character* c)  { prevChar = c;  }
        void  setYear     (u_int      y)  { year     = y;  }

    private:
        u_int           weight;
        u_int           year;
};

struct gameDistanceCompare
{
    bool operator() (Character* a, Character* b)
    {
        return a->distance > b->distance;
    }
};


struct gameYearCompare
{
    bool operator() (const Game* left, const Game* right) const
    {
        return (*left).getYear() < (*right).getYear();
    }
};

#endif
