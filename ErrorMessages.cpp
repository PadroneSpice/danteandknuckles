/**
 *  ErrorMessages.cpp
 *
 */
 #include "CharacterGraph.hpp"

/**
 * Unused / Undefined Error Codes:
 * 1 and 2: Originally used in CharacterGraph::generateWays() and CharacterGraph::load() to check whether
 *          a TSV line is in the correct format. No longer used because I want the data TSV to allow
 *          empty lines for readability; lines not in the character-game-year format are simply skipped.
 * 3 and 6
 */

void CharacterGraph::printErrorMessage(u_int codeNumber)
{
    if (codeNumber == 0)
    {
        std::cerr << "Error Code 0: Did not select (w)eighted / (u)nweighted." << std::endl;
    }

    if (codeNumber == 5)
    {
        std::cerr << "Error Code 5: Ostream can't be opened." << std::endl;
    }

    if (codeNumber == 7)
    {
        std::cerr << "Error Code 7: Empty data list." << std::endl;
    }

    else
    {
        std::cerr << "Error: Undefined error code number: " << codeNumber << std::endl;
    }
}


void CharacterGraph::printErrorMessage(u_int codeNumber, s_vector v)
{
    //u_int num;
    //if (codeNumber == 1) { num = DATA_CATEGORY_COUNT;  }  // wfLoad()
    //if (codeNumber == 2) { num = CHARACTER_PAIR_COUNT; }  // generateWays()
    //std::cerr << "Warning: Incorrectly formatted line detected. Expected "
    //          << num << " data: " << std::endl;
    //for (auto i = v.begin(); i != v.end(); ++i)
    //{
    //    std::cerr << *i << "\t";
    //}
    //std::cerr << std::endl;
}
