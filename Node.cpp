#include "Node.hpp"
#include "CharacterGraph.hpp"

 void Character::addAppearance(Game*& g)
 {
     si_pair p = std::make_pair(g->getName(), g->getYear());
     g_map::iterator got = this->appearances.find(p);
     if (got == this->appearances.end())
     {
         this->appearances.emplace(p, g);
     }
 }

 void Game::addChar(Character*& c)
 {
     c_map::iterator got = roster.find(c->getName());
     if (got == roster.end())
     {
         roster.emplace(c->getName(), c);
     }
 }