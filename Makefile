CC=g++
CXXFLAGS=-std=c++11 -Wall -pedantic
LDFLAGS=

# if passed "type=opt" at command-line, compile with "-O3" flag (otherwise use "-g" for debugging)

ifeq ($(type),opt)
	CPPFLAGS += -O3
	LDFLAGS += -O3
else
	CPPFLAGS += -g
	LDFLAGS += -g
endif

default: wayfinder

# This one only works if both wayfinder and characterConnections
# have the main() actually called main().
all: wayfinder characterConnections

wayfinder:
	g++  ModeSetup.cpp Node.cpp AlgorithmUtilities.cpp ErrorMessages.cpp SearchAlgorithms.cpp CharacterGraph.cpp Wayfinder.cpp -o wf

characterConnections:
	g++ ModeSetup.cpp Node.cpp AlgorithmUtilities.cpp ErrorMessages.cpp SearchAlgorithms.cpp CharacterGraph.cpp CharConnections.cpp -o cc

clean:
	rm -f *.o wayfinder characterConnections wf cc
