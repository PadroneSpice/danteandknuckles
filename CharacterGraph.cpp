/*
 * CharacterGraph.cpp
 */


#include "CharacterGraph.hpp"
using namespace Modes;

/** Constructor */
CharacterGraph::CharacterGraph()
{
    //just defaults so the compiler is okay
    taskMode   = Mode_Task::Task_Wayfinder;
    edgeMode   = Mode_Edge::Edge_Unweighted;
    searchMode = Mode_Search::Search_UF;
}

void CharacterGraph::load(const char* inputFile)
{
    sv_vector inputData = buildRecord(inputFile);
    for (auto d = inputData.begin(); d != inputData.end(); ++d)
    {
        s_string      character_condition = "";
        int           game_year = 0;

        // Use this for when a condition (such as Mii costume) is given.
        if (d->size() == DATA_CATEGORY_COUNT_CONDITION_GIVEN)
        {
            s_string      character_name = (*d)[0];
            s_string      game_name      = (*d)[1];
            try
            {
                game_year = stoi((*d)[2]);
                if  (game_year < 0) throw 1;
            }
            catch (...)
            {
                continue;
            }
            character_condition = (*d)[3];
            processData(character_name, game_name, game_year, character_condition);
            continue;
        }

        /* AKA System
         * When an AKA format line is found, add its contents to the akaMap.
         * The first name in the AKA string is the "base" name to be used for the search algorithms.
         * The base name's base name is itself.
         */
        if (d->size() == AKA_COLUMN_COUNT)
        {
            if ((*d)[0] != "[AKA]") { continue; }
            s_string lineToSplit = (*d)[1];
            s_vector aka_vec     = tokenizeString(lineToSplit);
            auto i = aka_vec.begin();
            s_string base_name = *i;
            while (i != aka_vec.end())
            {
                akaMap.insert(std::make_pair(*i, base_name));
                ++i;
            }
            continue;
        }

        // Skip over invalid lines.
        if (d->size() != DATA_CATEGORY_COUNT) { continue;}

        // Normal format lines.
        s_string  character_name  =  (*d)[0];
        s_string  game_name       =  (*d)[1];
        try
        {
            game_year = stoi((*d)[2]);
            if (game_year < 0) throw 1;
        }
        catch (...)
        {
            continue;
        }
        processData(character_name, game_name, game_year, character_condition);
    }
}

/* Connect the games to the characters incrementally.
 */
void CharacterGraph::connectCharsToGames(const char* inputFile, const char* outputFileName)
{
    i_ifstream                       infile (inputFile);
    o_ofstream                       ostream;
    sp_stack                         cps; //character pair stack

    // Step 1: Get the input pairs and put them into a stack.
    ostream.open(outputFileName, o_ofstream::out | o_ofstream::trunc);
    if (!ostream.is_open()) { printErrorMessage(5); exit(1); } //TODO: actually fill this error out

    ostream << "Character1\tCharacter2\tYear\n";   // header
    sv_vector record = buildRecord(inputFile);
    reverse(record.begin(), record.end());
    for (auto d = record.begin(); d != record.end(); ++d)
    {
        if ((*d).size() != CHARACTER_PAIR_COUNT) { continue; }

 
        // AKA System
        s_string charA = (*d)[0];
        s_string charB = (*d)[1];
        s_map::iterator it;
  
        // Push B before A so that when the stack is unloaded, the order will be A,B rather than B,A.
        it = akaMap.find(charB);
        if (it != akaMap.end())
        {
            if (charB != it->second)
            {
                akaStack.push(std::make_pair(charB, it->second));
                charB = it->second;
            }
            else
            {
                akaStack.push(std::make_pair("^SKIP^", "^SKIP^"));
            }
        }
        else
        {
            akaStack.push(std::make_pair("^SKIP^", "^SKIP^"));
        }

        it = akaMap.find(charA);
        if (it != akaMap.end())
        {
            if (charA != it->second)
            {
                akaStack.push(std::make_pair(charA, it->second));
                charA = it->second;
            }
            else
            {
                akaStack.push(std::make_pair("^SKIP^", "^SKIP^"));
            }
        }
        else
        {
            akaStack.push(std::make_pair("^SKIP^", "^SKIP^"));
        }


        cps.push(std::make_pair(charA, charB));
    }
    //reverseStack(cps);

    // Step 2: Put the gameMap contents into a gameList sorted by year.
    // Note: According to stack overflow, iterating through a map actually
    //       gives the original data and not copies.
    g_map::iterator gm = gameMap.begin();
    while (gm != gameMap.end())
    {
        auto insertionPoint = std::upper_bound(gameList.begin(), gameList.end(), gm->second, gameYearCompare());

        // This is used for the first push and the latest years.
        if (insertionPoint == gameList.end())  {  gameList.push_back(gm->second);  }
        else
        {
            gameList.insert(insertionPoint, gm->second);
        }
        gm++;
    }

    // Step 3: Process each pair individually, unless the gameList is empty.
    if (gameList.empty()) { printErrorMessage(7); return; }
    while (!cps.empty())
    {
        Character* ptr_charA = nullptr;
        Character* ptr_charB = nullptr;
        s_string name_charA  = cps.top().first;
        s_string name_charB  = cps.top().second;
        u_int currYear       = 0;
        bool connectionFound = false;
 
        // This both assigns the character pointers and handles absent characters.
        if (!hasChar(name_charA, ptr_charA) || !hasChar(name_charB, ptr_charB))
        {
            printUnion(name_charA, name_charB, ostream);
        }
        // If the pair is between the same character, just use 0 as the year.
        else if (name_charA == name_charB)
        {
            printUnion(name_charA, name_charB, 0, ostream);
        }
        else
        {
            // Push the first game onto the stack and take the game's year as current.
            gameStack.push(gameList[0]);
            currYear = gameList[0]->getYear();

            // Continue with the game list, skipping the first entry.
            g_vector::iterator g = gameList.begin();
            std::advance(g, 1);
            while (g != gameList.end())
            {
                // Check if this game's year is new. If the year is the same, just add the game to the stack.
                // If the year is different, push and empty the stack and then check for the connections
                // before adding the game to the stack and updating the current year.

                if ((*g)->getYear() == currYear)  { gameStack.push(*g); }
                else
                {
                    pushGameStackContents();

                    if ((searchMode == Mode_Search::Search_BFS && BFS(name_charA, name_charB, ostream, false))
                     || (searchMode == Mode_Search::Search_UF  && charsAreUnioned(ptr_charA, ptr_charB)     ))
                     {
                          connectionFound = true;
                          break;
                     }
                    else
                    {
                        gameStack.push(*g);
                        currYear = (*g)->getYear();
                    }
                }

                ++g;
            }

            // Handle the remaining stack contents and get their year using the return value.
            // If the connection still hasn't appeared, then there is no connection.
            if (!gameStack.empty())
            {
                currYear = pushGameStackContents();

                if ((searchMode == Mode_Search::Search_BFS && BFS(name_charA, name_charB, ostream, false))
                 || (searchMode == Mode_Search::Search_UF  && charsAreUnioned(ptr_charA, ptr_charB)     ))
                {    connectionFound = true;    }
            }



            s_string charPrintA = name_charA;
            s_string charPrintB = name_charB;

            // AKA System
            if (!akaStack.empty())
            {
                if (name_charA == akaStack.top().second)
                {
                    charPrintA = akaStack.top().first;
                    akaStack.pop();
                }
                else if (akaStack.top().first == "^SKIP^" && akaStack.top().second == "^SKIP^")
                {
                    akaStack.pop();
                }
                else
                {
                    akaStack.pop();
                }
            }
            if (!akaStack.empty())
            {
                if (name_charB == akaStack.top().second)
                {
                    charPrintB = akaStack.top().first;
                    akaStack.pop();
                }
                else if (akaStack.top().first == "^SKIP^" && akaStack.top().second == "^SKIP^")
                {
                    akaStack.pop();
                }
                else
                {
                    akaStack.pop();
                }
            }

            if (connectionFound) { printUnion(charPrintA, charPrintB, currYear, ostream); }
            else                 { printUnion(charPrintA, charPrintB,           ostream); }


            // Reset the resources so that subsequent searches aren't messed up.
            // BFS already calls resetForAlgorithms() at its end.
            if (searchMode == Mode_Search::Search_UF) { resetParentsInStack(parentStack); }
            resetCharAppearances(); 

        }
        cps.pop();
    }
    infile.close();
    ostream.close();
}


void CharacterGraph::generateWays(const char* inputPairFile, const char* outputFileName)
{
    o_ofstream     ostream;

    ostream.open(outputFileName, o_ofstream::out | o_ofstream::trunc);
    if (!ostream.is_open()) { printErrorMessage(5); exit(1); }
    ostream << "(character)--[game#@year]-->(game)--...\n"; // Write header.

    sv_vector outputData = buildRecord(inputPairFile);

    for (auto d = outputData.begin(); d != outputData.end(); ++d)
    {
        if ((*d).size() != CHARACTER_PAIR_COUNT) { continue; } // Just skip over unusable lines.

        s_string charA = (*d)[0];
        s_string charB = (*d)[1];

        // AKA System
        s_map::iterator it;
        it = akaMap.find(charA);
        if (it != akaMap.end())
        {
            if (charA != it->second)
            {
                akaList.push_back(std::make_pair(charA, it->second));
                charA = it->second;
            }
            else { akaList.push_back(std::make_pair(charA, charA)); }
        }
        it = akaMap.find(charB);
        if (it != akaMap.end())
        {
            if (charB != it->second)
            {
                akaList.push_back(std::make_pair(charB, it->second));
                charB = it->second;
            }
            else { akaList.push_back(std::make_pair(charB, charB)); }
        }

        // Find the way and write it to output.
        if (edgeMode == Mode_Edge::Edge_Weighted)   { Dijkstra(charA, charB, ostream);  }
        else                                        { BFS(charA, charB, ostream, true); }
    }
    ostream.close();
}

/* Destructor: Delete the games and the characters through the vectors */
CharacterGraph::~CharacterGraph()
{
  for (auto c = charMap.begin(); c != charMap.end(); ++c) { delete c->second; }
  for (auto g = gameMap.begin(); g != gameMap.end(); ++g) { delete g->second; }
}
