/**
 * Definitions for functions declared in CharacterGraph.hpp
 * by Sean Castillo
 */

#include "CharacterGraph.hpp"
#include <iostream>
#include <fstream>

/**
 *  Breadth-First Search -> Searches for path from character A to character B,
                            without regard to year weight.
 *
 *  How it works:
 *      - Check whether characters A and B are present in the graph.
 *      - Reset the conditions of the nodes as required for BFS.
 *      - Make a queue and perform the following action:
 *
 *      - Get all the characters connected to this character by its games.
 *        If a shorter distance has been found:
 *            -Update the pointers:
 *                  previous character of current game -> current character
 *                  previous game of latter character  -> current game
 *            -Update the record distance.
 *        If a detected character hasn't been in the queue before, put it in the queue.
 *
 *        Outer loop:  Games connected to the current character (g)
 *        Inner loop:  Characters connected to the current game (c)
 *     - If character B hasn't been visited, then return false.
 *     - If character B has been visited, then print the info (if requested) and return true.
*        Either way, resetForAlgorithms() is called.
 */
bool CharacterGraph::BFS(const s_string charA, const s_string charB,
                               o_ofstream& outputFile, bool handlePrint)
{

    //std::cout << "Running BFS on: " << charA << " and " << charB << std::endl;
    //std::cout << "checking the stats of each character first: " << std::endl;
    //for (auto s : charMap)
    //{
        //std::cout << "name: " << s.first << std::endl;
        //std::cout << "distance: " << s.second->distance << std::endl;
        //std::cout << "visited: " << s.second->visited << std::endl;
        //if (s.second->prevGame != nullptr)
        //{
            //std::cout << "prevGame: " << s.second->prevGame->getName() << std::endl;
        //}
        //else
        //{
            //std::cout << "prevGame: None" << std::endl;
        //}
        //std::cout << "---";
    //}


    auto a = charMap.find(charA);
    if (a == charMap.end()) { return false; }

    auto b = charMap.find(charB);
    if (b == charMap.end()) { return false; }

    c_queue      qbert;
    Character*   curr;
    a->second->setDistance(0);
    a->second->visited = true;
    visitedCharStack.push(a->second);
    qbert.push(a->second);


    while (!qbert.empty())
    {
        curr = qbert.front();
        qbert.pop();

        for (auto g : curr->appearances)
        {
            for (auto c : g.second->roster)
            {
                if ( (curr->distance + 1) < c.second->distance )
                {
                    g.second->prevChar = curr;
                    gamePrevCharStack.push(g.second);
                    c.second->prevGame =  (g.second);
                    c.second->distance = curr->distance + 1;
                }
                if (!(c.second->visited))
                {
                    (c.second)->visited = true;
                    visitedCharStack.push(c.second);
                    qbert.push(c.second);
                }
            }
        }
    }



    if (b->second->visited == false)
    {
        resetForAlgorithms();
        return false;
    }

    if (handlePrint) { printTheWay(b->second, charA, outputFile); }
    resetForAlgorithms();
    return true;
}

/**
 *  Dijkstra's Algorithm -> Searches for a path from character A to character B,
 *                          with weight for years.
 *  How it works:
 *      - Check whether characters A and B are present in the graph.
 *      - Reset the conditions of the nodes as required for Dijkstra's Algorithm.
 *      - Make a priority queue and perform the following action:
 *            - Place the first node in the queue.
 * Get all the characters connected to this character by its games.
 * If a shorter distance from character to character has been found on an unvisited node:
 *     Update the pointers: previous character of the game to current character
 *                          previous game of the latter character to current game
 *     Update the recorded distance.
 *     Put the character into the queue.
 * outer loop: games connected to that character
 * inner loop: characters connected to those games
 */
bool CharacterGraph::Dijkstra(const s_string charA, const s_string charB, o_ofstream& outputFile)
{
    auto ita = charMap.find(charA);
    if (ita == charMap.end()) { return false; }
    auto itb = charMap.find(charB);
    if (itb == charMap.end()) { return false; }
    resetForAlgorithms();

    std::priority_queue<Character*, std::vector<Character*>, gameDistanceCompare> pq;

    ita->second->setDistance(0);
    pq.emplace(ita->second);

    Character* curr;

    while (!pq.empty())
    {
        curr          = pq.top();
        curr->visited = true;
        visitedCharStack.push(curr);
        pq.pop();

        for (auto itg : curr->appearances)
        {
            for (auto itc : itg.second->roster)
            {
                u_int someDistance = curr->distance + itg.second->getWeight();
                if ( itc.second->distance > someDistance && itc.second->visited == false)
                {
                    itg.second->prevChar = curr;
                    gamePrevCharStack.push(itg.second);
                    itc.second->prevGame = itg.second;
                    itc.second->distance = someDistance;
                    pq.emplace(itc.second);
                }
            }
        }
    }
    if (itb->second->visited == false) { return false; }
    curr = itb->second;
    printTheWay(curr, charA, outputFile);

    return true;
}
