/**
 *  AlgorithmUtilities.cpp
 *  by Sean Castillo
 */

#include "CharacterGraph.hpp"

using namespace Modes;


s_vector  CharacterGraph::tokenizeString(const s_string inputString, const s_string del) // default del assigned as %
{
    s_vector splitStringVector;
    int start = 0;
    int end   = inputString.find(del);
    while (end != -1)
    {
        splitStringVector.push_back(inputString.substr(start, end - start));
        start = end + del.size();
        end = inputString.find(del, start);
    }
    return splitStringVector;
}

/* Stores the data from the input file into a vector that is returned.
 * This is used for both the character pairs file and the data file,
 * since both are TSVs. */
sv_vector CharacterGraph::buildRecord(const char* inputFile)
{
    sv_vector record;
    std::ifstream infile(inputFile);
    bool readHeader = false;
    s_string line;
    if (infile.is_open())
    {
        while (std::getline(infile, line))
        {
            if (!readHeader)
            {
                readHeader = true;
                continue;
            }

            s_stringstream ss(line);
            s_vector       datum;
            while (ss)
            {
                s_string next;
                if (!getline(ss, next, '\t')) break;
                datum.push_back(next);
            }
            record.push_back(datum);
        }
        infile.close();
    }
    return record;
}


/* Determines whether to add the character and/or the movie and updates info.
 * The difference is that CC must not have the character->game connections set here.
 * It's because the games have to be pushed into the graph one year at a time during CC.
 * The condition data is blank.
 */
 void CharacterGraph::processData(s_string characterName, s_string gameName, int gameYear, s_string characterCondition)
 {
     bool addCharacter = false;
     bool addGame = false;

     /* Check for the character. */
     auto ic = charMap.find(characterName);
     if (ic == charMap.end()) { addCharacter = true; }

     /* Check for the game. */
     auto ig = gameMap.find(std::make_pair(gameName, gameYear));
     if (ig == gameMap.end()) { addGame = true; }

     /* Now update the data as needed! */
     /* Case 1: The character and the game are both new data. */
     if (addCharacter && addGame)
     {
         Character* c = new Character(characterName);
         Game*      g = new Game(gameName, gameYear);
         if (taskMode == Mode_Task::Task_Wayfinder)
         {
             c->addAppearance(g);
             if (!characterCondition.empty())
             {
                 c->addAppearanceCondition(gameName, characterCondition); }
         }
         g->addChar(c);
         charMap.insert(std::make_pair(characterName, c));
         gameMap.insert(std::make_pair(std::make_pair(gameName, gameYear), g));
         return;
     }
     /* Case 2: Character is new. Game is not new. */
     else if (addCharacter && !addGame)
     {
         Character* c = new Character(characterName);
         if (taskMode == Mode_Task::Task_Wayfinder)
         {
             c->addAppearance(ig->second);
             if (!characterCondition.empty())
             { c->addAppearanceCondition(gameName, characterCondition); }
         }
         ig->second->addChar(c);
         charMap.insert(std::make_pair(characterName, c));
         return;
     }
     /* Case 3: Character is not new. Game is new. */
     else if (!addCharacter && addGame)
     {
         Game* g = new Game(gameName, gameYear);
         g->addChar(ic->second);
         if (taskMode == Mode_Task::Task_Wayfinder)
         {
             ic->second->addAppearance(g);
             if (!characterCondition.empty())
             { ic->second->addAppearanceCondition(gameName, characterCondition); }
         }
         gameMap.insert(std::make_pair(std::make_pair(gameName, gameYear), g));
         return;
     }
     /**
      * Case 4: Character is not new. Game is not new.
      */
     else if (!addCharacter && !addGame)
     {
         if (taskMode == Mode_Task::Task_Wayfinder)
         {
             ic->second->addAppearance(ig->second);
             if (!characterCondition.empty())
             { ic->second->addAppearanceCondition(gameName, characterCondition); }
         }
         ig->second->addChar(ic->second);
         return;
     }
 }


 /* The sentinel nodes are their own parents. */
 Character* CharacterGraph::unionFind(Character* c)
 {
     if (c->P != c) { c->P = unionFind(c->P); }
     return c->P;
 }

 bool CharacterGraph::charsAreUnioned(Character* a, Character* b)
 {

   Character* sentinelA = unionFind(a);
   Character* sentinelB = unionFind(b);
   if (sentinelA == sentinelB) { return true;  }
   else                        { return false; }
 }

void CharacterGraph::makeUnion(Character*& a, Character*& b)
{
    Character* sentinelA = unionFind(a);
    Character* sentinelB = unionFind(b);

    if (sentinelA == sentinelB) { return; }
    parentStack.push(sentinelA);
    parentStack.push(sentinelB);

    if (sentinelA->size < sentinelB->size)
    {
        sentinelA->P      = sentinelB;
        sentinelB->size  += sentinelA->size;
    }
    else
    {
      sentinelB->P       = sentinelA;
      sentinelA->size   += sentinelB->size;
  }
}

bool CharacterGraph::hasChar(s_string characterName) const
{
    auto c = charMap.find(characterName);
    if (c == charMap.end()) { return false; }
    else                    { return true;  }
}

/* version that updates the given pointer */
bool CharacterGraph::hasChar(const s_string characterName, Character*& CharPtr)
{
    auto c = charMap.find(characterName);
    if (c == charMap.end()) { return false; }
    else
    {
        CharPtr = c->second;
        return true;
    }
}

/* Reverse a stack. */
void CharacterGraph::reverseStack(sp_stack s)
{
    sp_stack tempStack;
    while (!s.empty())
    {
        auto data = s.top();
        s.pop();
        tempStack.push(data);
    }
    s = tempStack;
}

/* Pushes the current contents of gameStack to the graph and returns the year of the contents. */
u_int CharacterGraph::pushGameStackContents()
{
    u_int stackYear = gameStack.top()->getYear();
    while (!gameStack.empty())
    {
        Game* curr = gameStack.top();
        for (auto ita : curr->roster)
        {
            ita.second->addAppearance(curr);
            if (searchMode == Mode_Search::Search_UF)
            {
                for (auto itz : curr->roster) { makeUnion(ita.second, itz.second); }
            }
        }
        gameStack.pop();
    }
    return stackYear;
}

/* Resets the 'visited' status on all the characters in a given stack. */
void CharacterGraph::resetCharStackVisited(c_stack& c)
{
    while (!c.empty())
    {
        Character* curr = c.top();
        curr->visited   = false;
        c.pop();
    }
}

/* Resets the parents of the Character nodes in the given stack. */
void CharacterGraph::resetParentsInStack(c_stack& c)
{
    while (!c.empty())
    {
        Character* curr = c.top();
        curr->P         = curr;
        c.pop();
    }
}

/* Resets the Game appearances of every Character node in the graph. */
void CharacterGraph::resetCharAppearances()
{
    for (auto it = charMap.begin(); it != charMap.end(); ++it)
    {
        it->second->appearances.clear();
    }
}

/**
 * Resets the node stats important for BFS and Dijkstra:
 * -Mark all character nodes as unvisited, infinitely far, and having no prevGame.
 * -Mark all game nodes as having no prevCharacter,
 */
void CharacterGraph::resetForAlgorithms()
{
    while (!visitedCharStack.empty())
    {
        Character* curr = visitedCharStack.top();
        curr->setPrevGame(nullptr);
        curr->distance = MAX_DISTANCE;
        curr->visited = false;
        visitedCharStack.pop();
    }

    while (!gamePrevCharStack.empty())
    {
        Game* curr = gamePrevCharStack.top();
        curr->setPrevChar(nullptr);
        gamePrevCharStack.pop();
    }
}

/* Used by connectCharsToGames() */
void CharacterGraph::printUnion(const s_string charA, const s_string charB, const u_int year, o_ofstream& outputFile)
{
    outputFile << charA << '\t' << charB << '\t' << year << '\n';
    std::cout  << charA << "   "<< charB << "   "<< year << std::endl;
}

/* Version to use when the year is not given, i.e. was not found */
void CharacterGraph::printUnion(const s_string charA, const s_string charB, o_ofstream& outputFile)
{
    outputFile << charA << '\t'  << charB << '\t'  << 9999 << '\n';
    std::cout  << charA << "   " << charB << "   " << 9999 << std::endl;
}

/* Print to both the output file and to the terminal. 
 * This could be modified for SDL later. */
void CharacterGraph::printTheWay(Character* curr, const s_string charA, o_ofstream& outputFile)
{
    c_stack    c;
    g_stack    g;
    s_string   gameKeeper = "";
    Character* characterKeeper = nullptr;

    while (curr->getName() != charA)
    {
        c.push(curr);
        g.push(curr->prevGame);
        curr = curr->prevGame->prevChar;
    }

    c.push(curr);
    while (!c.empty())
    {
        s_string characterNameToPrint = c.top()->getName();
        for (auto& namePair : akaList) // AKA System
        {
            if (c.top()->getName() == namePair.second)
            {
                characterNameToPrint = namePair.first;
            }
        }
        outputFile << "(" << characterNameToPrint << ")";
        std::cout  << "(" << characterNameToPrint << ")";
        characterKeeper = c.top();
        c.pop();
        if (!g.empty())
        {
            s_string conditionAddition = "";
         
            auto s = g.top()->prevChar->appearanceConditions.begin();
            while (s != g.top()->prevChar->appearanceConditions.end())
            {
                if (s->first == g.top()->getName())
                {
                    conditionAddition.append(" [");
                    conditionAddition.append(s->second);
                    conditionAddition.append("]");
                }
                s++;
            }
            
          outputFile << conditionAddition << "--[" << g.top()->getName()
                  << "#@" << g.top()->getYear() << "]-->";
          std::cout  << conditionAddition << "--[" << g.top()->getName()
                  << "#@" << g.top()->getYear() << "]-->";
          gameKeeper = g.top()->getName();
          g.pop();
        }
    }
    
    // This allows the character at the end of a path to have its condition for its local game to be displayed.
    if (characterKeeper != nullptr)
    {
        auto s = characterKeeper->appearanceConditions.begin();
        while (s != characterKeeper->appearanceConditions.end())
        {
            if (s->first == gameKeeper)
            {
                outputFile << " [" << s->second << "]";
                std::cout  << " [" << s->second << "]";
            }
            s++;
        }
    }

    outputFile << '\n';
    outputFile << '\n';
    std::cout  << '\n';
    std::cout  << '\n';
}

void CharacterGraph::printVector(g_vector gv)
{
    for (u_int i = 0; i < gv.size(); i++)
    {
        std::cout << gv[i]->getName() << "    " << gv[i]->getYear() << std::endl;
    }
    std::cout << std::endl;
}