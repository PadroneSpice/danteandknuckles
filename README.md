## Six Degrees of Featuring Dante from the Devil May Cry Series & Knuckles ##
 by Sean Castillo

Version 2.0

### First and Foremost
This build currently has two main(), located in Wayfinder.cpp, CharConnections.cpp.
In order to get the program to run as described below:

1. If using Visual Studio on Windows, between the main() in Wayfinder
   and the main() in CharConnections, edit one to be named main()
   and edit the other to be named something else, like otherMain().
   The one you want to run is the one that must be named main().
2. For Linux/Mac OS X Terminal, there is a Makefile; see "How to Use"
   for details.


### Four Modes
1. Wayfinder: Unweighted
>Finds the way from Character A to Character B in the fewest degrees
by using Breadth-First Search.
2. Wayfinder: Weighted
>Finds the way from Character A to Character B, given that recent
connections are considered shorter connections.
Uses Dijkstra's Algorithm because it's ideal for weighted edges
(which are years of games' releases here).
The weight of a game path is: 1 + (Current Year - Year Game Came Out).
3. Character Connections: Breadth-First Search
>Determines the earliest year when two characters were connected using
> breadth-first search. Loads games of the same year, inserts the games
into the graph, and then checks for a connection from Character A to Character B.
(This is just for algorithm performance comparison from when this was a class project; it's slow, and there's no need to use it over Union Find.)
4. Character Connections: Union Find
>Determines the earliest year when two characters were connected using
union find / disjoint sets for super fast calculation.


### How To Use

## Using Visual Studio
Visual Studio can just run it normally in a properly configured project environment
by going to the menu option:
Debug->DanteAndKnuckles Debug Properties->Configuration Properties->Debugging->Command Arguments,
then setting the Command Arguments as described in the "Doing the Command Line Inputs" section,
and then build and run.
Note: "ProjectName Debug Properties is sometimes labelled "ProjectName Property Pages".

## Using the Makefile
To compile these programs on Linux/Mac OS X Terminal,
The Makefile may be used.
These are the commands (which may be used in the terminal on
the directory of this project):

Compiles wayfinder as wf:
>make wayfinder

Compiles characterConnections as cc:
>make characterConnections

Compiles both wayfinder as wf and characterConnections as cc.
This requires both programs to have main() named main():
>make all

Default; identical to the individual wayfinder command:
>make

Deletes wf and cc:
>make clean


## Doing the Command Line Inputs

Wayfinder takes in these arguments:

0. ./wf (only if using the makefile/terminal method)

1. data input file name

2. pair input file name

3. (w)eighted / (u)nweighted mode

4. output file name

Character Connections takes in these arguments:

0. ./cc (only if using the makefile/terminal method)

1. data input file name

2. pair input file name

3. bfs / ufind mode

4. output file name

About Part 0:
It's only for the makefile/terminal method because Visual Studio automatically
adds that argument.

Example:
>./wf tsv/game_casts.tsv tsv/game_pairs.tsv w test.txt

Note that the input files included are in a folder named tsv.

*Terminology*

data input file name
>A tab-separated values file (TSV) that contains data in the three columns:
character[tab]game containing that character[tab]year of that game
This program comes with game_casts.tsv for this purpose.


pair input file name
>A TSV that contains data in two columns:
>characterA[tab]characterB

Note that the first line of a TSV is skipped by this program because that
line should be the header instead of character / game data.

output file name
> The results go into a file with this chosen name. A .txt is preferred.

bfs
>Breadth-First Search Algorithm

ufind
>Union Find, also known as Disjoint Set

### About game_casts.tsv

- The format of a data line is:
CharacterName<tab>GameName<tab>GameYear<tab>Appearance Condition
The "Appearance Condition" section is optional.

- Many of the characters were added to certain games post-launch, or were added
to specific region or system versions. Those details may be added in the
Appearance Condition sections.
For example:
Sweet Tooth and Sir Daniel Fortesque are in Hot Shots Golf 2, but only
the American and European versions, which came out a year after the Japanese
version.
>- Soulcalibur II has Link on GameCube, Spawn on Xbox, and Heihachi on PS2.
   Tekken 3 released on PlayStation with the addition of Gon a year after
   the arcade version's launch.

- The games in this data set are defined solely by name and year of release,
   and implementing a record system for content updates would get too complicated
   for what it's worth. However, updates/publications with their own titles
   (such as Monster Hunter World: Iceborne and Persona 5 Royal) may be added as
   their own games. Additionally, the Appearance Condition system may provide details.

- Many of the character appearances are brief cameos, such as the spirits in
   Super Smash Bros. Ultimate, the trophies in the previous Smash games,
   and the character endings and cards in Ultimate Marvel vs. Capcom 3.
>- PlayStation All-Stars Battle Royale has Donte as a playable character
>and Dante as collectible icons.

- Games are assigned their original release year, as opposed to their
international release years.

- Game series with reused titles (such as Doom and Samurai Shodown) may distinguish
between the original and the newer entry just by year, or with a description
in parentheses.

- Games are listed as their English names whenever possible.

- A character known by multiple names may have each name registered in the
format: ' [AKA]<tab>Main Name%Name2%Name3%Name4%...%'
A '%' symbol is required after each name in order for the line to be parsed correctly.
>Example: [AKA]<tab>Joker%Ren Amamiya%Amamiya Ren%Akira Kurusu%Kurusu Akira%Protagonist (Persona 5)%

- Identical name issues are solved on a case-by-case basis. For example, Joker
from DC is called "The Joker" to be different from "Joker" of Persona 5.

- Character customization doesn't count as an "appearance" of a character,
except when a character is in a game specifically as a feature
within the customization system.
>Examples: The Mii costumes in Super Smash Bros. Ultimate and KOS-MOS in Soulcalibur III.

- M. Bison, Balrog, and Vega are primarily called their international names (Dictator, Boxer, and Claw).
>However, the AKA system allows alternate names to be accepted, such as M. Bison (Boxer) or Balrog (Claw).

## TO DO (maybe)

1. Make the Makefile better, as in properly compartmentalized.

2. Implement CTIME to make the current year always the true current year.

3. Do GUI with SDL.

4. Add a "No Smash games" mode because Smash with all cameos makes everything too easy.
