/**
 * ModeSetup.cpp
 * by Sean Castillo
 */

#include "CharacterGraph.hpp"
using namespace Modes;

void CharacterGraph::setTaskMode(Mode_Task mode)
{
    if (mode == Mode_Task::Task_Wayfinder)
    {
        taskMode = Mode_Task::Task_Wayfinder;
        return;
    }
    taskMode = Mode_Task::Task_CharacterConnections;
}

 void CharacterGraph::setSearchMode(s_string mode)
 {
     std::transform(mode.begin(), mode.end(), mode.begin(), ::tolower);

     if (mode == "bfs"
      || mode == "breadth-first search"
      || mode == "breadth first search")
     {
         searchMode = Mode_Search::Search_BFS;
         return;
     }
     if (mode == "ufind"
      || mode == "union find"
      || mode == "disjoint set")
     {
         searchMode = Mode_Search::Search_UF;
         return;
     }

     std::cout << mode
     << " not recognized as a search mode. Defaulting to union find mode."
               << std::endl;
     searchMode = Mode_Search::Search_UF;
 }

 void CharacterGraph::setEdgeMode(s_string mode)
 {
     std::transform(mode.begin(), mode.end(), mode.begin(), ::tolower);
     if (mode == "w"  || mode == "weighted"   )
     {
         std::cout<<"Weighted mode selected!"<<std::endl;
         edgeMode = Mode_Edge::Edge_Weighted;
         return;
     }
     if (mode == "u"  || mode == "unweighted" )
     {
         std::cout<<"Unweighted mode selected!"<<std::endl;
         edgeMode = Mode_Edge::Edge_Unweighted;
         return;
     }

    std::cout << mode
    << " not recognized as an edge mode. Defaulting to unweighted edge mode. "
    << std::endl;
    edgeMode = Mode_Edge::Edge_Unweighted;
 }
